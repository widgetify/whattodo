const mongoose = require('mongoose');

const ThingsTodoSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true
  },
});

const ThingsTodo = mongoose.model("thingstodo", ThingsTodoSchema);
module.exports = ThingsTodo;