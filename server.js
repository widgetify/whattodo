import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';
let urlencode = require('urlencode');
const CONNECTION_URL = "mongodb+srv://"+urlencode('julian')+":"+urlencode('roleMiddleware2004!')+"@widgetifycluster1-waqe5.gcp.mongodb.net/whattodo?retryWrites=true&w=majority";
mongoose.connect(CONNECTION_URL, {
  useNewUrlParser: true
});

//controllers
import todoController from './controllers/todo'

const app = express()

app.use(express.json())
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/api/v1/todos', todoController.createTodo);
app.get('/api/v1/todos', todoController.readAllTodos);
app.get('/api/v1/todos/:id', todoController.readTodo);
app.patch('/api/v1/todos/:id', todoController.updateTodo);
app.delete('/api/v1/todos/:id', todoController.deleteTodo);

app.get('/api/v1/getRandomTodo', todoController.getRandomTodo);

app.listen(3000)
console.log('app running on port ', 3000);