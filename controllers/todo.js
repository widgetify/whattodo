import thingstodo from '../db/db.js';
const thingstodoModel = require('../db/models/thingstodo');
const axios = require('axios')

const readAllTodos = async (req, res) => {
    const thingstodo = await thingstodoModel.find({});
    try {
        console.log(thingstodo)
        res.send(thingstodo);
    } catch (err) {
        res.status(500).send(err);
    }
}

const readTodo = async (req, res) => {
    const thingstodo = await thingstodoModel.findById(req.params.id);
    try {
        console.log(thingstodo)
        res.send(thingstodo);
    } catch (err) {
        res.status(500).send(err);
    }
}

const createTodo = async (req, res) => {
    const thingstodo = new thingstodoModel(req.body);
    try {
        await thingstodo.save();
        res.send(thingstodo);
    } catch (err) {
        res.status(500).send(err);
    }
}

const updateTodo = async (req, res) => {
    try {
        thingstodoModel.findOneAndUpdate(req.params.id, req.body, {new: true}, (err, doc) => {
            if (err) {
                console.log("Something wrong when updating data!");
            }
        
            res.send(doc)
        });
      } catch (err) {
        res.status(500).send(err)
      }
      
} 

const deleteTodo = async (req, res) => {
    try {
        const thingstodo = await thingstodoModel.findByIdAndDelete(req.params.id)
    
        if (!thingstodo) res.status(404).send("No item found")
        res.status(200).send()
      } catch (err) {
        res.status(500).send(err)
      }
}

const getRandomTodo = async (req, res) => {
    let type = req.query.type
    let participants = req.query.participants
    let params = {}
    params.type = type
    params.participants = participants
    //set param for highaccessibility
    if (req.query.highaccessibility) {
        params.minaccessibility = 0.5
    }
    //set param for lowpriced
    if (req.query.lowprice) {
        params.maxprice = 0.5
    }
    axios.get('http://www.boredapi.com/api/activity/', { params })
    .then(response => {
        console.log(response)
        res.send(response.data);
    })
    .catch(error => {
        console.log(error);
    });
}

export default {
    readAllTodos,
    readTodo,
    createTodo,
    updateTodo,
    deleteTodo,
    getRandomTodo
};